#include"Character.h"
//definitions for Class Character and child Classes

//Definitions for Character Class
Character::Character() //default constructor
{
	health = 10;
	name = "Villager";
}
Character::Character(int health, string name) //defined constructor
{
	setHealth(health);
	setName(name);
}

void Character::setHealth(int healthValue)//setter for health
{
	health = healthValue;
}
int Character::getHealth() //getter for health
{
	//cout << name << "'s health is" << health;
	return health;
}

void Character::setName(string charName) //setter for name
{
	name = charName;
}
string Character::getName() //getter for name
{
	//cout << "Character's name is " << name;
	return name;
}

void Character::displayInfo() //displays the data
{
	cout << "Character's name is " << name << "." << endl;
	cout << name << "'s health is" << health << "." << endl;
}

//Definitions for Goblin Class
Goblin::Goblin() //default constructor
{
	health = 4;
	name = "Wild Goblin";
	swarm = 1;
	attack = 2;
}
Goblin::Goblin(string gobName, int gobHealth, int swarmVal, int attackVal) //defined constructor
{
	setName(gobName);
	setHealth(gobHealth);
	setSwarm(swarmVal);
	setAttack(attackVal);
}

void Goblin::setSwarm(int swarmVal) //sets number of goblins in swarm
{
	swarm = swarmVal;
}
int Goblin::getSwarm()
{
	return swarm;
}

void Goblin::setAttack(int attackVal) //sets attack to swarm number
{
	attack = swarm * attackVal;
}
int Goblin::getAttack()
{
	return attack;
}

void Goblin::displayInfo() 
{
	cout << "This goblin's name is " << name << "." << endl;
	cout << name << "'s health is " << health << "." << endl;
	cout << name << " has a swarm of " << swarm << "." << endl;
	cout << name << "'s attack is therefore " << attack << "." << endl;
}

//Definitions for Slayer Class
Slayer::Slayer() //default constructor
{
	health = 20;
	name = "Mercenary";
	armor = 5;
	strength = 10;
}
Slayer::Slayer(string slayName, int slayHealth, int armorVal, int strengthVal) //defined constructor
{
	setName(slayName);
	setHealth(slayHealth);
	setArmor(armorVal);
	setStrength(strengthVal);
}

void Slayer::setArmor(int armorVal)
{
	armor = armorVal;
}
int Slayer::getArmor()
{
	return armor;
}

void Slayer::setStrength(int strengthVal)
{
	strength = strengthVal;
}
int Slayer::getStrength()
{
	return strength;
}

void Slayer::displayInfo()
{
	cout << "This slayer's name is " << name << "." << endl;
	cout << name << "'s health is " << health << "." << endl;
	cout << name << "'s armor is " << armor << " points strong." << endl;
	cout << name << " has a strength of " << strength << "." << endl;
}