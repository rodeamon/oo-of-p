/*
Roderick Eamon's code for C++2 Assignment 1.
*/
#pragma once
#include<string>
#include<iostream>
using namespace std;

class Character 
{
protected:
	int health;
	string name;

public:
	Character();
	Character(int, string);

	int getHealth();
	void setHealth(int);
	string getName();
	void setName(string);
	//void combat();
	void static displayInfo();
};

class Goblin : public Character 
{
private:
	int swarm;
	int attack;

public:
	Goblin();
	Goblin(string, int, int, int);

	int getSwarm();
	void setSwarm(int);
	int getAttack();
	void displayInfo();
	void setAttack(int);
	
};

class Slayer : public Character
{
private:
	int armor;
	int strength;

public:
	Slayer();
	Slayer(string, int, int, int);

	int getArmor();
	void setArmor(int);
	int getStrength();
	void displayInfo();
	void setStrength(int);
	
};